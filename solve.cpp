#include <bits/stdc++.h>

using namespace std;

#pragma region
// clang-format off
#define fastIO ios_base::sync_with_stdio(0); cin.tie(NULL); cout.tie(NULL);
#define int long long
#define newLine cout<<"\n";
#define fullLength(v) v.begin(), v.end()
#define rfullLength(v) v.rbegin(), v.rend()
#define fileIO freopen("input.txt", "r", stdin); freopen("output.txt", "w", stdout);

typedef long long ll;

const ll maxx = 1e9 + 9;
const string yes = "Yes\n", no = "No\n";

int lcm(int a, int b) {
  return (a * b) / __gcd(a, b);
}

bool isPrime(int num) {
  if (num < 2)
    return false;
  for (int x = 2; x * x <= num; x++) {
    if (num % x == 0)
      return false;
    }
  return true;
}

// clang-format on
#pragma endregion

int solve() {
  int n, x, c;
  vector<int> vb;
  vector<int>::iterator it;

  cin >> n;
  vector<int> va(n);

  

}

int32_t main() {

  // clang-format off
  fastIO
  #ifdef DEBUG_LOCALE
      readOnly
  #elif !defined (ONLINE_JUDGE)
      fileIO
  #endif

  int t = 1;

  cin >> t;
  while (t--)
  solve();

}
